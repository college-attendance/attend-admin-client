module Msgs exposing (Msg(..))

import Data.Student exposing (Student)
import Http
import Table
import Time


type Msg
    = SetQuery String
    | SetTableState Table.State
    | GotStudents (Result Http.Error { time : Time.Posix, students : List Student })
    | SelectStudent String
