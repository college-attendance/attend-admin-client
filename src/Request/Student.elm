module Request.Student exposing (getStudents)

import Data.Student exposing (Student)
import Http
import Json.Decode as Decode exposing (..)
import Json.Decode.Pipeline exposing (hardcoded, optional, required)
import Json.Encode as Encode
import Time exposing (millisToPosix)


getStudents : (Result Http.Error GetStudentsResult -> msg) -> Cmd msg
getStudents msg =
    let
        url =
            "/students/"

        request =
            Http.get url getStudentsDecoder
    in
    Http.send msg request


type alias GetStudentsResult =
    { time : Time.Posix
    , students : List Student
    }


getStudentsDecoder : Decoder GetStudentsResult
getStudentsDecoder =
    Decode.succeed GetStudentsResult
        |> required "time" unixTimeDecoder
        |> required "students" studentsDecoder


unixTimeDecoder : Decoder Time.Posix
unixTimeDecoder =
    map millisToPosix int


studentsDecoder : Decoder (List Student)
studentsDecoder =
    list studentDecoder


studentDecoder : Decoder Student
studentDecoder =
    Decode.succeed Student
        |> required "id" string
        |> required "name" string
        |> required "email" string
        |> required "version" int


rs : String -> Decoder (String -> b) -> Decoder b
rs key =
    required key string


ri : String -> Decoder (Int -> b) -> Decoder b
ri key =
    required key int


rf : String -> Decoder (Float -> b) -> Decoder b
rf key =
    required key float
