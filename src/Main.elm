module Main exposing (Model, config, init, main, update, view)

import Browser
import Css exposing (..)
import Data.Student exposing (Student)
import Dict exposing (Dict)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css, placeholder, type_)
import Html.Styled.Events exposing (onClick, onInput)
import Msgs exposing (Msg(..))
import Request.Student exposing (getStudents)
import Table
import Time


main : Program () Model Msg
main =
    Browser.element
        { view = view >> toUnstyled
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }



-- MODEL


type alias Model =
    { tableState : Table.State
    , query : String
    , students : Dict String Student
    , selectedStudent : Maybe String
    }


init : ( Model, Cmd Msg )
init =
    let
        model =
            { tableState = Table.initialSort "Year"
            , query = ""
            , students = Dict.empty
            , selectedStudent = Nothing
            }
    in
    ( model, getStudents GotStudents )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetQuery newQuery ->
            ( { model | query = newQuery }
            , Cmd.none
            )

        SetTableState newState ->
            ( { model | tableState = newState }
            , Cmd.none
            )

        GotStudents (Ok new) ->
            let
                newStudents =
                    List.foldl insertPort model.students new.students

                insertPort : Student -> Dict String Student -> Dict String Student
                insertPort student ports =
                    Dict.update student.id (insertIfNewer student) ports

                insertIfNewer newStudent maybeExistingPort =
                    case maybeExistingPort of
                        Nothing ->
                            Just newStudent

                        Just existingStudent ->
                            if existingStudent.version < newStudent.version then
                                Just newStudent

                            else
                                Just existingStudent
            in
            ( { model
                | students = newStudents
              }
            , Cmd.none
            )

        GotStudents (Err _) ->
            ( model
            , Cmd.none
            )

        SelectStudent id ->
            ( { model | selectedStudent = Just id }, Cmd.none )



-- VIEW


view : Model -> Html Msg
view { students, tableState, query, selectedStudent } =
    let
        acceptablePeople =
            List.filter (String.contains lowerQuery << String.toLower << .name) studentsList

        lowerQuery =
            String.toLower query

        studentsList =
            Dict.values students
    in
    div
        [ css
            [ displayFlex

            -- , height (vh 100)
            , padding2 (rem 2) (rem 0)
            , boxSizing borderBox
            ]
        ]
        [ div
            [ css [ marginLeft (rem 2) ] ]
            [ h1 [] [ text "Registered Students" ]
            , input [ placeholder "Search by Name", onInput SetQuery ] []
            , fromUnstyled <| Table.view config tableState acceptablePeople
            ]
        , div
            [ css
                [ position fixed
                , right (px 0)
                , top (rem 2)
                , bottom (rem 2)
                , width (px 500)
                , maxWidth (vw 95)
                , padding2 (rem 1) (rem 2)
                , borderRadius4 (rem 1) (rem 0) (rem 0) (rem 1)
                , backgroundColor (hsla 208 0.39 0.23 0.1)
                , overflow scroll
                , marginLeft auto
                ]
            ]
            (case selectedStudent of
                Nothing ->
                    [ h1 [] [ text "Select a Student" ]
                    ]

                Just id ->
                    [ h1 [] [ text "Select a Student" ]
                    ]
             -- case
             -- let maybeStudent =
             -- in case maybeStudent
            )
        ]


config : Table.Config Student Msg
config =
    Table.config
        { toId = .name
        , toMsg = SetTableState
        , columns =
            [ Table.stringColumn "Name" .name
            , Table.stringColumn "Email" .email
            , Table.veryCustomColumn
                { name = ""
                , viewData = viewSelector
                , sorter = Table.unsortable
                }
            ]
        }


viewSelector : Student -> Table.HtmlDetails Msg
viewSelector { id } =
    Table.HtmlDetails []
        [ toUnstyled <| span [ onClick (SelectStudent id), css [ cursor pointer, color (hex "4A90E2") ] ] [ text "Edit" ]
        ]
