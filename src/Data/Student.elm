module Data.Student exposing (Student)


type alias Student =
    { id : String
    , name : String
    , email : String
    , version : Int
    }
